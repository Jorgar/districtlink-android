package com.jordigarcia.securityapp.app_security.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.jordigarcia.securityapp.app_security.Adapters.RecyclerViewContentDetailsAdapter;
import com.jordigarcia.securityapp.app_security.City;
import com.jordigarcia.securityapp.app_security.District;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.Utils.Constants;
import com.jordigarcia.securityapp.app_security.Utils.File;

import java.util.ArrayList;
import java.util.List;

import static com.jordigarcia.securityapp.app_security.Utils.Constants.DISTRICT_TYPE_HOLDER;

public class DetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private FloatingActionButton floatingActionButtonMap;
    private CardView cardViewHeader;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private FragmentManager fragmentManager;
    private Context mContext;
    private RecyclerView rvCitiesDistricts;
    private RecyclerViewContentDetailsAdapter recyclerViewContentDetailsAdapter;
    private ProgressBar progressBarMap;
    private FrameLayout mapContainer;
    private CardView linkCardView;

    //layout design components
    private ImageView backgroundHeader;
    private ImageView iconDetails;
    private TextView title, subtitle;
    private CardView cityCard;
    private CardView contentCardList;
    private TextView titleContentListSection;
    private TextView titleCommentsSection;
    private ImageView iconLeftMap;
    private ImageView iconRightMap;
    private TextView titleLeftMap;
    private TextView titleRightMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_details_activity);

        floatingActionButtonMap = (FloatingActionButton)findViewById(R.id.fb_map);
        cardViewHeader = (CardView)findViewById(R.id.cw_city);
        backgroundHeader = (ImageView)findViewById(R.id.background_image_city);
        rvCitiesDistricts = (RecyclerView)findViewById(R.id.rv_content_details);
        progressBarMap = (ProgressBar)findViewById(R.id.map_progress_bar_details);
        mapContainer = (FrameLayout)findViewById(R.id.map_details_container);
        linkCardView = (CardView)findViewById(R.id.cardview_link_other_details);
        iconDetails = (ImageView)findViewById(R.id.icon_vh);
        title = (TextView)findViewById(R.id.title_text_vw) ;
        subtitle = (TextView)findViewById(R.id.subtitle_text_vw);
        cityCard = (CardView)findViewById(R.id.cardview_link_other_details);
        contentCardList = (CardView)findViewById(R.id.cardview_content_list_details);
        titleContentListSection = (TextView)findViewById(R.id.content_details_title_rv);
        titleCommentsSection = (TextView)findViewById(R.id.title_all_comments);
        iconLeftMap = (ImageView)findViewById(R.id.icon_left_map);
        iconRightMap = (ImageView)findViewById(R.id.icon_right_map);
        titleLeftMap = (TextView)findViewById(R.id.title_map_left);
        titleRightMap = (TextView)findViewById(R.id.title_right_map);

        mapContainer.setAlpha(0f);
        rvCitiesDistricts.setHasFixedSize(true);

        mContext = this;

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        rvCitiesDistricts.setLayoutManager(layoutManager);

        recyclerViewContentDetailsAdapter = new RecyclerViewContentDetailsAdapter(this, loadDistricts() ,DISTRICT_TYPE_HOLDER);
        rvCitiesDistricts.setAdapter(recyclerViewContentDetailsAdapter);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        fragmentManager = getSupportFragmentManager();
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_details);
        mapFragment.getMapAsync(this);

        floatingActionButtonMap.hide();

        final byte[] bytes = this.getIntent().getByteArrayExtra("background_image_bytes");
        final Bitmap[] bmp = new Bitmap[1];

        new AsyncTask<Integer, Void, Void>(){
            @Override
            protected Void doInBackground(Integer... params) {
                bmp[0] = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                backgroundHeader.setScaleType(ImageView.ScaleType.CENTER_CROP);
                backgroundHeader.setImageBitmap(bmp[0]);
            }
        }.execute();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                floatingActionButtonMap.show();
                floatingActionButtonMap.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.anim_floatin_button_in));
            }
        }, 400);

        floatingActionButtonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Vote fragment to show - TO DO", Toast.LENGTH_SHORT).show();
            }
        });

        linkCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "Link activity to show - TO DO", Toast.LENGTH_SHORT).show();
            }
        });

        configurePageLayout(this.getIntent().getIntExtra("type_holder", -1));

    }

    @Override
    public void onBackPressed() {
        finish();
        this.overridePendingTransition(0, android.R.anim.fade_out);
        //super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                AlphaAnimation animation1 = new AlphaAnimation(0f, 1.0f);
                animation1.setDuration(500);
                mapContainer.setAlpha(1f);
                mapContainer.startAnimation(animation1);
                progressBarMap.setVisibility(View.GONE);
                //This will be dynamic in the future. It will depend on the city selected n+and get the bounds from the cloud
                LatLngBounds inverness = new LatLngBounds(
                        new LatLng(57.429762, -4.308378), new LatLng(57.540671, -4.082472));

                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(false);
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(inverness, 1));
            }
        });

        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Intent intent = new Intent(mContext, MapActivity.class);
                intent.putExtra("map_type", setMapType(getIntent().getIntExtra("type_holder", -1)));
                mContext.startActivity(intent);
            }
        });
    }

    private List<Object> loadDistricts(){
        List<Object> districts = new ArrayList<>();
        for (int i = 0; i < 20;i++){
            districts.add(new District("Merkinch","INVERNESS_MERKINCH", null, 0, null));
        }
        return districts;
    }

    public void configurePageLayout(int type){
        switch (type){
            case Constants.DISTRICT_ACTIVITY:
                iconDetails.setImageDrawable(mContext.getResources().getDrawable(R.drawable.district_icon_list));
                subtitle.setVisibility(View.VISIBLE);
                cityCard.setVisibility(View.VISIBLE);
                contentCardList.setVisibility(View.GONE);
                titleCommentsSection.setText( getResources().getString(R.string.comments_section_district));
                iconRightMap.setImageDrawable(mContext.getResources().getDrawable(R.drawable.cities_icon_map_district_page));
                titleRightMap.setText("Inverness");
                break;
            case Constants.CITY_ACTIVITY:
                iconDetails.setImageDrawable(mContext.getResources().getDrawable(R.drawable.cities_icon_vh));
                subtitle.setVisibility(View.GONE);
                cityCard.setVisibility(View.GONE);
                contentCardList.setVisibility(View.VISIBLE);
                titleCommentsSection.setText( getResources().getString(R.string.comments_section_city));
                titleContentListSection.setText(getResources().getString(R.string.list_section_city));
                break;
            case Constants.COUNTRY_ACTIVITY:
                iconDetails.setImageDrawable(mContext.getResources().getDrawable(R.drawable.countries_icon_list));
                subtitle.setVisibility(View.GONE);
                cityCard.setVisibility(View.GONE);
                contentCardList.setVisibility(View.VISIBLE);
                titleCommentsSection.setText( getResources().getString(R.string.comments_section_country));
                titleContentListSection.setText(getResources().getString(R.string.list_section_country));
                iconLeftMap.setVisibility(View.INVISIBLE);
                titleLeftMap.setVisibility(View.INVISIBLE);
                iconRightMap.setImageDrawable(mContext.getResources().getDrawable(R.drawable.cities_icon_map_district_page));
                break;
            default:
                break;
        }
    }

    public int setMapType(int type) {
        switch (type) {
            case Constants.DISTRICT_ACTIVITY:
                return Constants.MAP_DISTRICT;
            case Constants.CITY_ACTIVITY:
                return Constants.MAP_CITY;
            case Constants.COUNTRY_ACTIVITY:
                return Constants.MAP_COUNTRY;
            default:
                return -1;
        }
    }

}
