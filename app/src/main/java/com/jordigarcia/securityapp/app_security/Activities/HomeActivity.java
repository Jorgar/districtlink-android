package com.jordigarcia.securityapp.app_security.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.UserState;
import com.jordigarcia.securityapp.app_security.Utils.Constants;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private CardView buttonCities, buttonDistricts, buttonCountries, buttonProfile;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home_activity);

        buttonCities = (CardView)findViewById(R.id.button_cities);
        buttonDistricts = (CardView)findViewById(R.id.button_districts);
        buttonCountries = (CardView)findViewById(R.id.button_countries);
        buttonProfile = (CardView)findViewById(R.id.button_profile);

        buttonCities.setOnClickListener(this);
        buttonDistricts.setOnClickListener(this);
        buttonCountries.setOnClickListener(this);
        buttonProfile.setOnClickListener(this);

        if(UserState.getInstance().getLogState() == Constants.USER_NOT_REGISTERED){
            ImageView iconProfile = (ImageView)findViewById(R.id.icon_profile_home);
            TextView profileTitle = (TextView)findViewById(R.id.profile_textview_home);
            iconProfile.setColorFilter(this.getResources().getColor(R.color.deactivated_icon));
            profileTitle.setTextColor(this.getResources().getColor(R.color.deactivated_icon));
            buttonProfile.setClickable(false);
            buttonProfile.setCardBackgroundColor(this.getResources().getColor(R.color.deactivated_card));
            buttonProfile.setOnTouchListener(null);
        }

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id){
            case R.id.button_cities:
                Intent intentCities = new Intent(this, MapListActivity.class);
                intentCities.putExtra("id_type_list", Constants.LIST_CITY);
                this.startActivity(intentCities);
                //Prevent activity transition animation. It causes bad performance with the recyclerview animation.
                //overridePendingTransition(0, 0);
                break;
            case R.id.button_districts:
                Intent intentDistricts = new Intent(this, MapListActivity.class);
                intentDistricts.putExtra("id_type_list", Constants.LIST_DISTRICT);
                this.startActivity(intentDistricts);
                break;
            case R.id.button_countries:
                Intent intentCountries = new Intent(this, MapListActivity.class);
                intentCountries.putExtra("id_type_list", Constants.LIST_COUNTRY);
                this.startActivity(intentCountries);
                break;
            case R.id.button_profile:
                Intent intentProfile = new Intent(this, ProfileActivity.class);
                this.startActivity(intentProfile);
                break;
            default:
                break;
        }
    }
}
