package com.jordigarcia.securityapp.app_security.Activities;


import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jordigarcia.securityapp.app_security.Fragments.SignUpFragment;
import com.jordigarcia.securityapp.app_security.Fragments.ForgotPasswordDialogFragment;
import com.jordigarcia.securityapp.app_security.Fragments.LogInFragment;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.UserState;
import com.jordigarcia.securityapp.app_security.Utils.Animation;
import com.jordigarcia.securityapp.app_security.Utils.Constants;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView loginButton, accountButton, forgotPassword;
    private LinearLayout skipButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppStyleDistrictLink);
        setContentView(R.layout.layout_launch_activity);
        loginButton = (TextView)findViewById(R.id.textview_log_in);
        accountButton = (TextView)findViewById(R.id.textview_sign_up);
        skipButton = (LinearLayout) findViewById(R.id.skip_text);

        accountButton.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        skipButton.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        //Handle every click event on LoginActivity
        int id = view.getId();
        Intent intent = new Intent(this, HomeActivity.class);

        switch (id){
            case R.id.textview_log_in:
                Animation.loadAnimatedFragment(this,R.id.sign_up_holder,new LogInFragment(),R.anim.anim_fragment_register_in,R.anim.anim_fragment_register_out);
                break;
            case R.id.textview_sign_up:
                Animation.loadAnimatedFragment(this,R.id.sign_up_holder,new SignUpFragment(),R.anim.anim_fragment_register_in,R.anim.anim_fragment_register_out);
                break;
            case R.id.skip_text:
                UserState.getInstance().setLogState(Constants.USER_NOT_REGISTERED);
                this.startActivity(intent);
                break;
            default:
                return;
        }
    }
}
