package com.jordigarcia.securityapp.app_security.Activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.jordigarcia.securityapp.app_security.City;
import com.jordigarcia.securityapp.app_security.Utils.Constants;
import com.jordigarcia.securityapp.app_security.District;
import com.jordigarcia.securityapp.app_security.Fragments.DistrictFragment;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.Utils.File;

import java.util.List;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private FragmentManager fragmentManager;
    private Context mContext;
    private ImageView header;
    private Polygon activePolygon;

    private int colorDistrictStroke;
    private int mapType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.layout_map_activity);
        fragmentManager = getSupportFragmentManager();
        colorDistrictStroke = getResources().getColor(R.color.bc_color_vh_city);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mapType = getIntent().getIntExtra("map_type", -1);

        //remove status and navigations darkness when calling drawer layout
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
         //       | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(fragmentManager.getBackStackEntryCount()==0){
            try{
                activePolygon.setStrokeWidth(3f);
            }catch (Exception e){
                System.out.println(e.toString());
            }
            mMap.getUiSettings().setAllGesturesEnabled(true);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //This will be dynamic in the future. It will depend on the city selected n+and get the bounds from the cloud
        LatLngBounds inverness = new LatLngBounds(
                new LatLng(57.429762, -4.308378), new LatLng(57.540671, -4.082472));

        mMap.setPadding(0,70,0,0);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(inverness, 1));
        Log.d("Location", mMap.isMyLocationEnabled()+"");

        Gson gson = new Gson();
        City city = gson.fromJson(File.readJSONFromAsset(this, "districts-json/UK_INVERNESS_DISTRICTS.json"), City.class);

        for (District district: city.getDistricts()) {
            PolygonOptions polygonOptions = new PolygonOptions();
            for(int i = 0; i < district.getCoordinates().size(); i++){
                polygonOptions.add(new LatLng(district.getCoordinates().get(i)[0], district.getCoordinates().get(i)[1]));
            }
            polygonOptions.strokeWidth(3f);
            switch (district.getSecurityLevel()){
                case Constants.UNSAFE_SECURITY_LEVEL:
                    polygonOptions.strokeColor(colorDistrictStroke).fillColor(Constants.FILL_RED);
                    break;
                case Constants.MEDIUM_SECURITY_LEVEL:
                    polygonOptions.strokeColor(colorDistrictStroke).fillColor(Constants.FILL_YELLOW);
                    break;
                case Constants.SAFE_SECURITY_LEVEL:
                    polygonOptions.strokeColor(colorDistrictStroke).fillColor(Constants.FILL_GREEN);
                    break;
            }

            Polygon districtPolygon = mMap.addPolygon(polygonOptions);
            districtPolygon.setClickable(true);
            districtPolygon.setTag(district);
        }


        mMap.setOnPolygonClickListener(new GoogleMap.OnPolygonClickListener() {
            @Override
            public void onPolygonClick(final Polygon polygon) {
                if(fragmentManager.getBackStackEntryCount()>0){
                    fragmentManager.popBackStack();
                }

                //mMap.getUiSettings().setAllGesturesEnabled(false);

                if(activePolygon!=null){
                    activePolygon.setStrokeWidth(3f);
                }
                //Makes bigger stroke on selected polygon
                polygon.setStrokeWidth(8f);
                activePolygon = polygon;

                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(getDistrictBounds(polygon), 1),500, new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        mMap.animateCamera(CameraUpdateFactory.zoomOut(), 500, new GoogleMap.CancelableCallback() {
                            @Override
                            public void onFinish() {
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int height = size.y;
                                mMap.animateCamera(CameraUpdateFactory.scrollBy(0, height/3),500,null);

                                //Show DistrictFragment
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                District district = (District) polygon.getTag();
                                Bundle bundle = new Bundle();
                                bundle.putString("district_name", district.getDistrictName());
                                bundle.putInt("district_security_level", district.getSecurityLevel());
                                DistrictFragment fragment = new DistrictFragment();
                                fragment.setArguments(bundle);
                                fragmentTransaction.setCustomAnimations(R.anim.anim_fragment_district_in, R.anim.anim_fragment_district_out,
                                        R.anim.anim_fragment_district_in, R.anim.anim_fragment_district_out);

                                fragmentTransaction.add(R.id.distric_fragment, fragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();
                            }

                            @Override
                            public void onCancel() {

                            }
                        });
                    }

                    @Override
                    public void onCancel() {

                    }
                });

            }
        });
    }

    public LatLngBounds getDistrictBounds(Polygon polygon){
        List<LatLng> points = polygon.getPoints();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(int i = 0 ; i < points.size() ; i++)
        {
            builder.include(points.get(i));
        }
        LatLngBounds bounds = builder.build();

        return bounds;
    }

}
