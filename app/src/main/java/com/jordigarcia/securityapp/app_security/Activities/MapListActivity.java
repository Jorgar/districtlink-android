package com.jordigarcia.securityapp.app_security.Activities;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jordigarcia.securityapp.app_security.Adapters.RecyclerViewListMapAdapter;
import com.jordigarcia.securityapp.app_security.City;
import com.jordigarcia.securityapp.app_security.Country;
import com.jordigarcia.securityapp.app_security.District;
import com.jordigarcia.securityapp.app_security.Fragments.CountrySelectorDialogFragment;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.Utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class MapListActivity extends AppCompatActivity {

    private RecyclerView recyclerViewListMaps;
    private RecyclerViewListMapAdapter recyclerViewListMapAdapter;
    private List<Object> contentList;
    private EditText searchText;
    private int listType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_list_map_activity);
        Intent intent = getIntent();
        listType = intent.getIntExtra("id_type_list", Constants.LIST_CITY);

        searchText = (EditText)findViewById(R.id.edittext_search_list);
        recyclerViewListMaps = (RecyclerView)findViewById(R.id.rv_map_list);
        ImageView iconCountry = (ImageView)findViewById(R.id.icon_country_list_map);

        recyclerViewListMaps.setHasFixedSize(true);
        contentList = new ArrayList<>();

        TextView header = (TextView)findViewById(R.id.title_list);
        EditText searchBar = (EditText)findViewById(R.id.edittext_search_list);

        switch (listType){
            case Constants.LIST_CITY:
                header.setText("Cities");
                searchBar.setHint("Search city");
                loadCities();
                break;
            case Constants.LIST_DISTRICT:
                header.setText("Districts");
                searchBar.setHint("Search district");
                loadDistricts();
                break;
            case Constants.LIST_COUNTRY:
                header.setText("Countries");
                searchBar.setHint("Search country");
                loadCountries();
                break;
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewListMaps.setLayoutManager(layoutManager);
        recyclerViewListMapAdapter = new RecyclerViewListMapAdapter(this, this, contentList);
        ScaleInAnimationAdapter alphaAdapter = new ScaleInAnimationAdapter(recyclerViewListMapAdapter);
        alphaAdapter.setFirstOnly(false);
        recyclerViewListMaps.setAdapter(alphaAdapter);

        iconCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CountrySelectorDialogFragment dialog = new CountrySelectorDialogFragment();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "country_selector_fragment");
            }
        });

        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String text = searchText.getText().toString().toLowerCase(Locale.getDefault());
                    recyclerViewListMapAdapter.filter(text);
                }
                return false;
            }
        });

    }

    public void loadCities(){
        for(int i = 0; i<50; i++){
            contentList.add(new City("Inverness",null, ResourcesCompat.getDrawable(getResources(), R.drawable.inverness, null)));
        }
    }

    public void loadDistricts(){
        City inverness = new City("Inverness",null, ResourcesCompat.getDrawable(getResources(), R.drawable.inverness, null));
        for(int i = 0; i<50; i++) {
            contentList.add(new District("Merkinch",null, null, 0, inverness));
        }
    }

    public void loadCountries(){
        for(int i = 0; i<50; i++) {
            contentList.add(new Country("United Kingdom", ResourcesCompat.getDrawable(getResources(), R.drawable.united_kindom_image, null)));
        }
    }
}
