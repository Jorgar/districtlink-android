package com.jordigarcia.securityapp.app_security.Activities;

import android.app.FragmentTransaction;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jordigarcia.securityapp.app_security.Fragments.CreditCardPaymentDialogFragment;
import com.jordigarcia.securityapp.app_security.Fragments.ForgotPasswordDialogFragment;
import com.jordigarcia.securityapp.app_security.R;

import org.w3c.dom.Text;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener{

    private FloatingActionButton backButton;
    private ImageView googlePayButton, creditCardButton, paypalButton;
    private TextView priceText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_payment_activity);

        backButton = (FloatingActionButton)findViewById(R.id.back_button_payment);
        googlePayButton = (ImageView)findViewById(R.id.imageview_googlepay);
        creditCardButton = (ImageView)findViewById(R.id.imageview_credit_card);
        paypalButton = (ImageView)findViewById(R.id.imageview_paypal);
        priceText = (TextView)findViewById(R.id.textview_price);

        backButton.setOnClickListener(this);
        googlePayButton.setOnClickListener(this);
        creditCardButton.setOnClickListener(this);
        paypalButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_button_payment:
                super.onBackPressed();
                break;
            case R.id.imageview_googlepay:
                Toast.makeText(this, "Google Pay Process", Toast.LENGTH_SHORT).show();
                break;
            case R.id.imageview_credit_card:
                CreditCardPaymentDialogFragment dialog = new CreditCardPaymentDialogFragment();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "creditcard_fragment");
                break;
            case R.id.imageview_paypal:
                Toast.makeText(this, "Paypal Process", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }
}
