package com.jordigarcia.securityapp.app_security.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.jordigarcia.securityapp.app_security.Adapters.ScreenSlidePagerAdapter;
import com.jordigarcia.securityapp.app_security.Fragments.DetailsFragment;
import com.jordigarcia.securityapp.app_security.Fragments.HistoryFragment;
import com.jordigarcia.securityapp.app_security.Fragments.PremiumFragment;
import com.jordigarcia.securityapp.app_security.Fragments.SignUpFirstFragment;
import com.jordigarcia.securityapp.app_security.Fragments.SignUpSecondFragment;
import com.jordigarcia.securityapp.app_security.NoSwipeableViewPager;
import com.jordigarcia.securityapp.app_security.R;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProfileActivity extends AppCompatActivity {

    private NoSwipeableViewPager viewPager;
    private List<Fragment> fragmentList = new ArrayList<>();

    private RoundedImageView profileImage;
    private RelativeLayout darkLayerProfileImage;
    private ImageView editImageButton;
    private boolean isEditingProfileImageActive = false;

    public static final int PICK_IMAGE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_profile_activity);

        profileImage = (RoundedImageView)findViewById(R.id.user_image_profile);
        darkLayerProfileImage = (RelativeLayout)findViewById(R.id.dark_layer_user_image);
        editImageButton = (ImageView)findViewById(R.id.button_edit_image);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        viewPager = (NoSwipeableViewPager)findViewById(R.id.viewpager_profile);
        ScreenSlidePagerAdapter vPagerAdapter = new ScreenSlidePagerAdapter(this, getSupportFragmentManager(), fragmentList);

        fragmentList.add(new DetailsFragment());
        fragmentList.add(new HistoryFragment());
        fragmentList.add(new PremiumFragment());

        viewPager.setAdapter(vPagerAdapter);

        ImageView buttonBack = (ImageView)findViewById(R.id.button_back_profile);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        editImageButton.setClickable(false);

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isEditingProfileImageActive){
                    isEditingProfileImageActive = true;
                    darkLayerProfileImage.setVisibility(View.VISIBLE);
                    AlphaAnimation animationOn = new AlphaAnimation(0.0f, 1.0f);
                    animationOn.setDuration(200);
                    darkLayerProfileImage.startAnimation(animationOn);
                    editImageButton.setClickable(true);

                    new AsyncTask<Integer, Void, Void>(){
                        @Override
                        protected Void doInBackground(Integer... params) {
                            try {
                                Thread.sleep(1500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            AlphaAnimation animationOn = new AlphaAnimation(1.0f, 0.0f);
                            animationOn.setDuration(200);
                            darkLayerProfileImage.startAnimation(animationOn);
                            darkLayerProfileImage.setVisibility(View.INVISIBLE);
                            editImageButton.setClickable(false);
                            isEditingProfileImageActive = false;
                        }
                    }.execute();
                }
            }
        });

        editImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TO DO - OPEN GALLERY IN A MORE FANCY WAY. NOW IS TO ABRUPT.
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select profile picture"), PICK_IMAGE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                profileImage.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_profile_details:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_profile_history:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_profile_premium:
                    viewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        }
    };

}
