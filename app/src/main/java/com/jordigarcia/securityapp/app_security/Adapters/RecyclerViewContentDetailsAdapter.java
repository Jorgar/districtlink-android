package com.jordigarcia.securityapp.app_security.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jordigarcia.securityapp.app_security.City;
import com.jordigarcia.securityapp.app_security.District;
import com.jordigarcia.securityapp.app_security.R;

import java.util.ArrayList;
import java.util.List;

import static com.jordigarcia.securityapp.app_security.Utils.Constants.CITY_TYPE_HOLDER;
import static com.jordigarcia.securityapp.app_security.Utils.Constants.DISTRICT_TYPE_HOLDER;

/**
 * Created by Jordi on 13/04/2018.
 */

public class RecyclerViewContentDetailsAdapter extends RecyclerView.Adapter<RecyclerViewContentDetailsAdapter.ContentHolder>{

    private Context mContext;
    private List<Object> content = new ArrayList<>();
    private int typeContentAdapter;

    private final int BODY = 1;
    private final int HEADER = 2;

    public RecyclerViewContentDetailsAdapter(Context mContext, List<Object> content, int typeContentAdapter){
        this.mContext = mContext;
        this.content = content;
        this.typeContentAdapter = typeContentAdapter;
    }

    @Override
    public ContentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_header_districts, parent, false);
                return new ContentHolder(view);

            default:
                //Setting the Body view...
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_rview_districts, parent, false);
                return new ContentHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ContentHolder holder, int position) {
        if(position>0) {
            switch (typeContentAdapter){
                case CITY_TYPE_HOLDER:
                    final City city = (City)content.get(position);
                    holder.itemView.setTag(position);
                    //holder.contentIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.icon_rv_city_details));
                    holder.holder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Open city Activity
                            Toast.makeText(mContext, "Open " + city.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
                case DISTRICT_TYPE_HOLDER:
                    final District district = (District) content.get(position);
                    holder.itemView.setTag(position);
                    holder.title.setText(district.getDistrictName());
                    holder.contentIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.icon_rv_district_details));
                    holder.holder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Open city Activity
                            Toast.makeText(mContext, "Open " + district.getDistrictName(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER;
        }

        return BODY;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return content.size();
    }

    public class ContentHolder extends RecyclerView.ViewHolder{

        private CardView holder;
        private TextView title;
        private ImageView contentIcon;

        public ContentHolder(View view) {
            super(view);

            title = (TextView)view.findViewById(R.id.title_content_details);
            contentIcon = (ImageView)view.findViewById(R.id.icon_district_rv_details);
            holder = (CardView)view.findViewById(R.id.card_content_details);
        }

    }

}
