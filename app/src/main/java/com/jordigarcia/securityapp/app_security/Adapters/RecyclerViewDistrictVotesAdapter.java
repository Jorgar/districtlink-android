package com.jordigarcia.securityapp.app_security.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.DistrictComment;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Jordi on 27/02/2018.
 */

public class RecyclerViewDistrictVotesAdapter extends RecyclerView.Adapter<RecyclerViewDistrictVotesAdapter.DistrictViewHolder> {

    private Context mContext;
    /*Do this in the future with custom classes(HistoryVotes, Users,etc) and cloud data*/
    private List<DistrictComment> votes = new ArrayList<>();

    private final int BODY = 1;
    private final int HEADER = 2;

    public RecyclerViewDistrictVotesAdapter(Context mContext, List<DistrictComment> votes) {
        this.mContext = mContext;
        this.votes = votes;
    }

    @Override
    public DistrictViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_header_listvotes_list, parent, false);
                return new DistrictViewHolder(view);

            default:
                //Setting the Body view...
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_rview_list_vote, parent, false);
                return new DistrictViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(DistrictViewHolder holder, int position) {
        if(position>0) {
            DistrictComment vote = votes.get(position);
            holder.itemView.setTag(position);
            holder.nameUser.setText(vote.getUserName());
            holder.accountType.setText(vote.getPublicationTime());
            holder.profileImage.setImageDrawable(vote.getProfileImage());
            holder.voteImage.setImageDrawable(vote.getVoteImage());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER;
        }

        return BODY;
    }

    @Override
    public int getItemCount() {
        return votes.size();
    }

    public class DistrictViewHolder extends RecyclerView.ViewHolder {

        private TextView nameUser, accountType;
        private CircleImageView profileImage;
        private ImageView voteImage;

        public DistrictViewHolder(View view) {
            super(view);

            nameUser = (TextView)view.findViewById(R.id.textview_username);
            accountType = (TextView)view.findViewById(R.id.textview_time_comment);
            profileImage = (CircleImageView)view.findViewById(R.id.profile_image);
            voteImage = (ImageView)view.findViewById(R.id.imageview_vote);
        }
    }
}
