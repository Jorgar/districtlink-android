package com.jordigarcia.securityapp.app_security.Adapters;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jordigarcia.securityapp.app_security.Utils.Constants;
import com.jordigarcia.securityapp.app_security.HistoryEntry;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.TitleHistory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jordi on 02/04/2018.
 */

public class RecyclerViewHistoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context mContext;
    /*Do this in the future with bbdd cloud data*/
    private List<Object> historyEntries = new ArrayList<>();

    private final int HEADER = 0;
    private final int TITLE = 1;
    private final int BODY = 2;

    public RecyclerViewHistoryListAdapter(Context mContext, List<Object> historyEntries) {
        this.mContext = mContext;
        this.historyEntries = historyEntries;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_header_history_list, parent, false);
                return new TitleSectionHolder(view);
            case TITLE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_title_list_history, parent, false);
                return new TitleSectionHolder(view);
            case BODY:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_rview_list_history, parent, false);
                return new HistoryViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(historyEntries.get(position) instanceof TitleHistory && position > 0) {

            TitleHistory entryData = (TitleHistory) historyEntries.get(position);
            holder.itemView.setTag(position);
            holder = (TitleSectionHolder) holder;
            ((TitleSectionHolder) holder).title.setText(entryData.getTitle());

        }else if(historyEntries.get(position) instanceof HistoryEntry && position > 0){

            HistoryEntry entryData = (HistoryEntry) historyEntries.get(position);
            holder = (HistoryViewHolder) holder;

            holder.itemView.setTag(position);
            ((HistoryViewHolder) holder).title.setText(entryData.getTitle());
            ((HistoryViewHolder) holder).subtitle.setText(entryData.getSubtitle());
            ((HistoryViewHolder) holder).datePosted.setText(entryData.getDate());

            switch (entryData.getType()){
                case Constants.HOLDER_TYPE_HISTORY_CITY:
                    ((HistoryViewHolder) holder).icon.setImageDrawable(ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.cities_icon_history, null));
                    break;
                case Constants.HOLDER_TYPE_HISTORY_DISTRICT:
                    ((HistoryViewHolder) holder).icon.setImageDrawable(ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.district_icon_history, null));
                    break;
                case Constants.HOLDER_TYPE_HISTORY_COUNTRY:
                    ((HistoryViewHolder) holder).icon.setImageDrawable(ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.countries_icon_history, null));
                    ((HistoryViewHolder) holder).subtitle.setVisibility(View.GONE);
                    break;
            }

            ((HistoryViewHolder) holder).editIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(mContext, "Edit entry clicked!", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return historyEntries.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER;
        }else if(historyEntries.get(position) instanceof TitleHistory){
            return TITLE;
        }else if(historyEntries.get(position) instanceof HistoryEntry){
            return BODY;
        }

        return 0;
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder {

        private TextView title, subtitle, datePosted;
        private ImageView icon, editIcon;

        public HistoryViewHolder(View view) {
            super(view);

            title = (TextView)view.findViewById(R.id.title_holder_history);
            subtitle = (TextView)view.findViewById(R.id.subtitle_holder_history);
            datePosted = (TextView)view.findViewById(R.id.date_posted_histroy);
            icon = (ImageView)view.findViewById(R.id.icon_holder_history);
            editIcon = (ImageView)view.findViewById(R.id.icon_edit_history);
        }
    }

    public class TitleSectionHolder extends RecyclerView.ViewHolder {

        private TextView title;

        public TitleSectionHolder(View view) {
            super(view);

            title = (TextView)view.findViewById(R.id.title_section_rv_list_history);
        }
    }
}
