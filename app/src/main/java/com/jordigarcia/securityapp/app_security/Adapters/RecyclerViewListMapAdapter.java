package com.jordigarcia.securityapp.app_security.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.jordigarcia.securityapp.app_security.Activities.DetailsActivity;
import com.jordigarcia.securityapp.app_security.City;
import com.jordigarcia.securityapp.app_security.Country;
import com.jordigarcia.securityapp.app_security.District;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.Utils.Constants;
import com.jordigarcia.securityapp.app_security.Utils.Screen;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jordi on 13/02/2018.
 */

public class RecyclerViewListMapAdapter extends RecyclerView.Adapter<RecyclerViewListMapAdapter.MapViewHolder>{

    private Context mContext;
    private Activity activity;
    private List<Object> contentList;
    private List<Object> contentListCopy = new ArrayList<>();
    private int lastAnimatedPosition = -1;

    private final int HEADER = -1;

    public RecyclerViewListMapAdapter(Context mContext, Activity activity, List<Object> contentList) {
        this.mContext = mContext;
        this.contentList = contentList;
        contentListCopy.addAll(contentList);
        this.activity = activity;
        setHasStableIds(true);
    }

    @Override
    public MapViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_rview_map_header, parent, false);
                return new MapViewHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_rview_map, parent, false);
                return new MapViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final MapViewHolder holder, int position) {
        runEnterAnimation(holder.itemView, position);
        if(position > 0) {

            if(contentList.get(position)instanceof District){

                final District district = (District) contentList.get(position);
                holder.itemView.setTag(position);
                holder.Title.setText(district.getDistrictName());
                holder.subInfo.setText(district.getCity().getTitle());
                holder.Image.setImageDrawable(district.getCity().getBackground());
                holder.icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.district_icon_list));

                holder.holder.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void onClick(View view) {
                        // TO DO
                        //open the map activity with the proper city coordinates.

                        new AsyncTask<Integer, Void, Void>(){

                            byte[] bytes;

                            @Override
                            protected Void doInBackground(Integer... params) {
                                Bitmap bitmap = ((BitmapDrawable) district.getCity().getBackground()).getBitmap();

                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 95, stream);
                                bytes = stream.toByteArray();
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                Intent intent = new Intent(mContext, DetailsActivity.class);
                                intent.putExtra("background_image_bytes",bytes);
                                intent.putExtra("type_holder", Constants.DISTRICT_ACTIVITY);

                                Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(
                                        (Activity) mContext,
                                        holder.holder,
                                        holder.holder.getTransitionName())
                                        .toBundle();

                                mContext.startActivity(intent,bundle);
                            }
                        }.execute();
                    }
                });

            }else if (contentList.get(position) instanceof City){

                final City city = (City)contentList.get(position);
                holder.itemView.setTag(position);
                holder.Title.setText(city.getTitle());
                holder.subInfo.setVisibility(View.GONE);
                holder.Image.setImageDrawable(city.getBackground());
                holder.icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.cities_icon_vh));

                holder.holder.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void onClick(View view) {
                        // TO DO
                        //open the map activity with the proper city coordinates.

                        new AsyncTask<Integer, Void, Void>(){

                            byte[] bytes;

                            @Override
                            protected Void doInBackground(Integer... params) {
                                Bitmap bitmap = ((BitmapDrawable) city.getBackground()).getBitmap();

                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 95, stream);
                                bytes = stream.toByteArray();
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                Intent intent = new Intent(mContext, DetailsActivity.class);
                                intent.putExtra("background_image_bytes",bytes);
                                intent.putExtra("type_holder", Constants.CITY_ACTIVITY);

                                Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(
                                        (Activity) mContext,
                                        holder.holder,
                                        holder.holder.getTransitionName())
                                        .toBundle();

                                mContext.startActivity(intent,bundle);
                            }
                        }.execute();
                    }
                });

            }else if (contentList.get(position) instanceof Country){

                final Country country = (Country)contentList.get(position);
                holder.itemView.setTag(position);
                holder.Title.setText(country.getName());
                holder.subInfo.setVisibility(View.GONE);
                holder.Image.setImageDrawable(country.getBackground());
                holder.icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.countries_icon_list));

                holder.holder.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void onClick(View view) {
                        // TO DO
                        //open the map activity with the proper city coordinates.

                        new AsyncTask<Integer, Void, Void>(){

                            byte[] bytes;

                            @Override
                            protected Void doInBackground(Integer... params) {
                                Bitmap bitmap = ((BitmapDrawable) country.getBackground()).getBitmap();

                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 95, stream);
                                bytes = stream.toByteArray();
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                Intent intent = new Intent(mContext, DetailsActivity.class);
                                intent.putExtra("background_image_bytes",bytes);
                                intent.putExtra("type_holder", Constants.COUNTRY_ACTIVITY);

                                Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(
                                        (Activity) mContext,
                                        holder.holder,
                                        holder.holder.getTransitionName())
                                        .toBundle();

                                mContext.startActivity(intent,bundle);
                            }
                        }.execute();
                    }
                });

            }


        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return HEADER;
        }

        if(contentList.get(position) instanceof District){
            return Constants.HOLDER_TYPE_LIST_DISTRICT;
        }else if (contentList.get(position) instanceof  City){
            return Constants.HOLDER_TYPE_LIST_CITY;
        }else if (contentList.get(position) instanceof Country){
            return Constants.HOLDER_TYPE_LIST_COUNTRY;
        }

        return -2;
    }

    @Override
    public int getItemCount() {
        return contentList.size();
    }

    public void filter(String text) {
        contentList.clear();
        if (text.isEmpty()) {
            contentList.addAll(contentListCopy);
        } else {
            text = text.toLowerCase();
            for (Object item : contentListCopy) {

                if((item instanceof District
                        && ((District)item).getDistrictName().toLowerCase().contains(text)
                    || (item instanceof City
                        && ((City)item).getTitle().toLowerCase().contains(text))
                    || (item instanceof Country
                        && ((Country)item).getName().toLowerCase().contains(text)))){

                        contentList.add(item);

                }

            }
        }
        notifyDataSetChanged();
    }

    private void runEnterAnimation(View view, int position) {
        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(Screen.getScreenHeight(mContext));
            view.animate()
                    .translationY(0)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(2000)
                    .start();
        }
    }

    public class MapViewHolder extends RecyclerView.ViewHolder {

        private CardView holder;
        private TextView Title;
        private TextView subInfo;
        private ImageView Image;
        private ImageView icon;

        public MapViewHolder(View view) {
            super(view);

            holder = (CardView)view.findViewById(R.id.cw_city);
            Title = (TextView)view.findViewById(R.id.title_text_vw);
            subInfo = (TextView)view.findViewById(R.id.subtitle_text_vw);
            Image = (ImageView)view.findViewById(R.id.background_city);
            icon = (ImageView)view.findViewById(R.id.icon_vh);
        }
    }
}

