package com.jordigarcia.securityapp.app_security.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.TitleCountry;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewSelectorListAdapter extends RecyclerView.Adapter<RecyclerViewSelectorListAdapter.SelectorHolder>{

    private Context mContext;
    private List<TitleCountry> content = new ArrayList<>();

    public RecyclerViewSelectorListAdapter(Context mContext, List<TitleCountry> content){
        this.mContext = mContext;
        this.content = content;
    }

    @Override
    public SelectorHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SelectorHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_country_item_dialog_selector, parent, false));
    }

    @Override
    public void onBindViewHolder(SelectorHolder holder, int position) {
        final TitleCountry item = content.get(position);
        holder.itemView.setTag(position);
        holder.title.setText(item.getTitleCountry());
        holder.countryIcon.setImageDrawable(item.getIconCountry());
        holder.holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, item.getTitleCountry() + " clicked!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return content.size();
    }


    public class SelectorHolder extends RecyclerView.ViewHolder{

        private TextView title;
        private ImageView countryIcon;
        private LinearLayout holder;

        public SelectorHolder(View view) {
            super(view);

            title = (TextView)view.findViewById(R.id.country_title_selector);
            countryIcon = (ImageView)view.findViewById(R.id.country_icon_selector);
            holder = (LinearLayout)view.findViewById(R.id.holder_title_contry_selector);
        }

    }

}
