package com.jordigarcia.securityapp.app_security.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by Jordi on 15/03/2018.
 */

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> fragments;
    private final Context context;

    /**
     * Constructor
     * @param context The context
     * @param fragmentManager The fragment manager
     * @param fragments The fragments
     * */
    public ScreenSlidePagerAdapter(final Context context, final FragmentManager fragmentManager,
                                   final List<Fragment> fragments) {
        super(fragmentManager);
        this.context = context;
        this.fragments = fragments;
    }



    @Override
    public Fragment getItem(final int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
