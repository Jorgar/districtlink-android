package com.jordigarcia.securityapp.app_security;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

public class City {

    private String title;
    private String subtitle;
    private String cityName;
    private String cityId;
    private District[] districts;
    private Drawable background;
    private int securityLevel;
    private ArrayList<Double[]> coordinates = new ArrayList<>();


    public City(String title, String subtitle, Drawable background){
        this.title = title;
        this.subtitle = subtitle;
        this.background = background;
    }

    public City(String title, String subtitle, String cityName, String cityId, Drawable background, ArrayList<Double[]> coordinates) {
        this.title = title;
        this.subtitle = subtitle;
        this.cityName = cityName;
        this.cityId = cityId;
        this.background = background;
        this.coordinates = coordinates;
    }

    public City(District[] districts) {
        this.districts = districts;
    }

    public District[] getDistricts() {
        return districts;
    }

    public void setDistricts(District[] districts) {
        this.districts = districts;
    }

    public City(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getBackground() {
        return background;
    }

    public void setBackground(Drawable background) {
        this.background = background;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public ArrayList<Double[]> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Double[]> coordinates) {
        this.coordinates = coordinates;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(int securityLevel) {
        this.securityLevel = securityLevel;
    }
}
