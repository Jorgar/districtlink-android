package com.jordigarcia.securityapp.app_security;

import android.graphics.drawable.Drawable;

public class Country {

    private String name;
    private Drawable background;

    public Country(String name, Drawable background) {
        this.name = name;
        this.background = background;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getBackground() {
        return background;
    }
}
