package com.jordigarcia.securityapp.app_security;

import java.util.ArrayList;

/**
 * Created by Jordi on 18/02/2018.
 */

public class District {

    private String districtName;
    private String districtId;
    private ArrayList<Double[]> coordinates = new ArrayList<>();
    private int securityLevel;
    private City city;

    public District(String districtName, String districtId, ArrayList<Double[]> coordinates, int securityLevel, City city) {
        this.districtName = districtName;
        this.districtId = districtId;
        this.coordinates = coordinates;
        this.securityLevel = securityLevel;
        this.city = city;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public ArrayList<Double[]> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Double[]> coordinates) {
        this.coordinates = coordinates;
    }

    public int getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(int securityLevel) {
        this.securityLevel = securityLevel;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
