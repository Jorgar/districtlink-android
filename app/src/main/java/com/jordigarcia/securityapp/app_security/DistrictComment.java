package com.jordigarcia.securityapp.app_security;

import android.graphics.drawable.Drawable;

/**
 * Created by Jordi on 27/02/2018.
 */

public class DistrictComment {

    private String userName, publicationTime, textComment;
    private int districtVote;
    private Drawable profileImage, voteImage;

    public DistrictComment(String userName, String publicationTime, String textComment, int districtVote, Drawable profileImage, Drawable voteImage) {
        this.userName = userName;
        this.publicationTime = publicationTime;
        this.textComment = textComment;
        this.districtVote = districtVote;
        this.profileImage = profileImage;
        this.voteImage = voteImage;
    }

    public String getTextComment() {
        return textComment;
    }

    public void setTextComment(String textComment) {
        this.textComment = textComment;
    }

    public int getDistrictVote() {
        return districtVote;
    }

    public void setDistrictVote(int districtVote) {
        this.districtVote = districtVote;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPublicationTime() {
        return publicationTime;
    }

    public void setPublicationTime(String publicationTime) {
        this.publicationTime = publicationTime;
    }

    public Drawable getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(Drawable profileImage) {
        this.profileImage = profileImage;
    }

    public Drawable getVoteImage() {
        return voteImage;
    }

    public void setVoteImage(Drawable voteImage) {
        this.voteImage = voteImage;
    }
}
