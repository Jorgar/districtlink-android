package com.jordigarcia.securityapp.app_security.Fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jordigarcia.securityapp.app_security.Adapters.RecyclerViewSelectorListAdapter;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.TitleCountry;

import java.util.ArrayList;
import java.util.List;

public class CountrySelectorDialogFragment extends DialogFragment {

    private List<TitleCountry> countries = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog_selector, container, false);

        ImageView closeButton = (ImageView) v.findViewById(R.id.close_button_selector);
        ImageView iconCountry = (ImageView) v.findViewById(R.id.icon_country_selected);
        RecyclerView countriesRecyclerView = (RecyclerView) v.findViewById(R.id.dialog_fragment_selector);
        loadCountries();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        countriesRecyclerView.setLayoutManager(layoutManager);
        RecyclerViewSelectorListAdapter adapter = new RecyclerViewSelectorListAdapter(getActivity(), countries);
        countriesRecyclerView.setAdapter(adapter);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

        return v;
    }

    public void loadCountries(){
        for(int i = 0; i<50; i++){
            countries.add(new TitleCountry("United Kingdom", ResourcesCompat.getDrawable(getResources(), R.drawable.uk_flag, null)));
        }
    }

}
