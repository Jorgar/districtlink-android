package com.jordigarcia.securityapp.app_security.Fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.jordigarcia.securityapp.app_security.R;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CreditCardPaymentDialogFragment extends DialogFragment {

    private Spinner yearSpinner, monthSpinner;
    private Button confirmPaymentButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog_credit_card_payment, container, false);

        yearSpinner = (Spinner) v.findViewById(R.id.spinner_year);
        monthSpinner = (Spinner) v.findViewById(R.id.spinner_month);
        confirmPaymentButton = (Button)v.findViewById(R.id.button_confirm_payment);

        List<Integer> yearData = getArrayYear();

        ArrayAdapter<Integer> yearSpinnerAdapter = new ArrayAdapter<Integer>(getContext(), R.layout.spinner_item, yearData);
        yearSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item);
        yearSpinner.setAdapter(yearSpinnerAdapter);

        List<String> monthData = getArrayMonth();

        ArrayAdapter<String> monthSpinnerAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, monthData);
        monthSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item);
        monthSpinner.setAdapter(monthSpinnerAdapter);

        confirmPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Credit Card Payment", Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
            }
        });

        return v;
    }

    public List<Integer> getArrayYear(){
        List<Integer> yearArray = new ArrayList<Integer>();

        for (int i = Calendar.getInstance().get(Calendar.YEAR); i < Calendar.getInstance().get(Calendar.YEAR) + 30; i++){
            yearArray.add(i);
        }

        return yearArray;
    }

    public List<String> getArrayMonth(){
        List<String> monthArray = new ArrayList<String>();
        DateFormatSymbols symbols = new DateFormatSymbols();
        String[] monthNames = symbols.getMonths();

        for (String month:monthNames) {
            monthArray.add(month);
        }

        return monthArray;
    }

}
