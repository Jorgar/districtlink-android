package com.jordigarcia.securityapp.app_security.Fragments;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jordigarcia.securityapp.app_security.Activities.LoginActivity;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.Utils.Screen;

/**
 * Created by Jordi on 01/04/2018.
 */

public class DetailsFragment extends Fragment{

    private Context mContext;
    private ImageView editEmail, editPassword;
    private ImageView confirmEditEmail, confirmEditPassword;
    private ImageView cancelEditEmail, cancelEditPassword;
    private LinearLayout editMenuEmail, editMenuPassword;
    private EditText editTextEmail, ediTextPassword;
    private TextView logOffText;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_details_profile, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = view.getContext();
        editEmail = (ImageView)view.findViewById(R.id.button_edit_email);
        editPassword = (ImageView)view.findViewById(R.id.button_edit_password);
        confirmEditEmail = (ImageView)view.findViewById(R.id.img_confirm_change_email);
        confirmEditPassword = (ImageView)view.findViewById(R.id.img_confirm_change_password);
        cancelEditEmail = (ImageView)view.findViewById(R.id.img_cancel_change_email);
        cancelEditPassword = (ImageView)view.findViewById(R.id.img_cancel_change_password);
        editMenuEmail = (LinearLayout)view.findViewById(R.id.edit_menu_email);
        editMenuPassword = (LinearLayout)view.findViewById(R.id.edit_menu_password);
        editTextEmail = (EditText)view.findViewById(R.id.email_details);
        ediTextPassword = (EditText)view.findViewById(R.id.password_details);
        logOffText = (TextView)view.findViewById(R.id.textview_log_out);

        editTextEmail.setEnabled(false);
        ediTextPassword.setEnabled(false);

        editTextEmail.clearFocus();
        ediTextPassword.clearFocus();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        editEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editEmail.setVisibility(View.GONE);
                editMenuEmail.setVisibility(View.VISIBLE);
                editTextEmail.setEnabled(true);
                editTextEmail.requestFocus();
                Screen.showKeyboard(getActivity());
            }
        });

        editPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editPassword.setVisibility(View.GONE);
                editMenuPassword.setVisibility(View.VISIBLE);
                ediTextPassword.setEnabled(true);
                ediTextPassword.requestFocus();
                Screen.showKeyboard(getActivity());
            }
        });

        confirmEditEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editEmail.setVisibility(View.VISIBLE);
                editMenuEmail.setVisibility(View.GONE);
                editTextEmail.setEnabled(false);
                editTextEmail.clearFocus();
                Toast.makeText(getContext(), "Email address updated", Toast.LENGTH_SHORT).show();
                //send the new data to the bbdd
            }
        });

        cancelEditEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editEmail.setVisibility(View.VISIBLE);
                editMenuEmail.setVisibility(View.GONE);
                editTextEmail.setEnabled(false);
                editTextEmail.clearFocus();
                //reset original email
            }
        });

        confirmEditPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editPassword.setVisibility(View.VISIBLE);
                editMenuPassword.setVisibility(View.GONE);
                ediTextPassword.setEnabled(false);
                ediTextPassword.clearFocus();
                Toast.makeText(getContext(), "Password updated", Toast.LENGTH_SHORT).show();
                //send the new data to the bbdd
            }
        });

        cancelEditPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editPassword.setVisibility(View.VISIBLE);
                editMenuPassword.setVisibility(View.GONE);
                ediTextPassword.setEnabled(false);
                ediTextPassword.clearFocus();
                //reset original password
            }
        });

        logOffText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Reset the app
                Intent i = new Intent(getContext(), LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });


    }

}
