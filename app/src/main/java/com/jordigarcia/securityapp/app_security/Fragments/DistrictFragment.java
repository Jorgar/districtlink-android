package com.jordigarcia.securityapp.app_security.Fragments;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jordigarcia.securityapp.app_security.Adapters.RecyclerViewDistrictVotesAdapter;
import com.jordigarcia.securityapp.app_security.Utils.Constants;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.DistrictComment;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class DistrictFragment extends Fragment {

    private String districtName;
    private int securityLevel;
    private RecyclerView recyclerViewListVotes;
    private RecyclerViewDistrictVotesAdapter recyclerViewListMapAdapter;
    private FloatingActionButton floatingActionButtonComment;
    private List<DistrictComment> votes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_district_menu, container, false);
        String strtext = getArguments().getString("district_name");
        securityLevel = getArguments().getInt("district_security_level");
        districtName = strtext;

        //RecyclerView
        recyclerViewListVotes = (RecyclerView)rootView.findViewById(R.id.rv_district_votes);
        recyclerViewListVotes.setHasFixedSize(true);

        votes = new ArrayList<>();
        loadVotes();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewListVotes.setLayoutManager(layoutManager);

        recyclerViewListMapAdapter = new RecyclerViewDistrictVotesAdapter(getContext(), votes);
        ScaleInAnimationAdapter alphaAdapter = new ScaleInAnimationAdapter(recyclerViewListMapAdapter);
        alphaAdapter.setFirstOnly(false);
        recyclerViewListVotes.setAdapter(alphaAdapter);

        recyclerViewListVotes.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    // Scrolling up
                    Log.d("SCROLL_RV","Scrolling up...");
                    floatingActionButtonComment.hide();
                } else {
                    // Scrolling down
                    floatingActionButtonComment.show();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView districtTextView = (TextView) view.findViewById(R.id.textview_district_name);
        districtTextView.setText(districtName);

        floatingActionButtonComment = (FloatingActionButton)view.findViewById(R.id.new_comment_map);

        floatingActionButtonComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(getContext(), "comment and vote, clicked", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        //ImageView districtSecurityImage = (ImageView) view.findViewById(R.id.image_state_district);
        /*switch (securityLevel){
            case 0:
                districtSecurityImage.setColorFilter(Color.RED);
                break;
            case 1:
                districtSecurityImage.setColorFilter(getResources().getColor(R.color.dark_yellow));
                break;
            case 2:
                districtSecurityImage.setColorFilter(getResources().getColor(R.color.green));
                break;
        }*/
    }

    public void loadVotes(){
        for (int i=0;i<250;i++){
            votes.add(new DistrictComment("Username", "1h ago", null, Constants.GOOD_VALORATION_DISTRICT, ResourcesCompat.getDrawable(getResources(), R.drawable.portrait_default, null),
                    ResourcesCompat.getDrawable(getResources(), R.drawable.icon_like_map, null)));
            votes.add(new DistrictComment("Username", "3h ago", null, Constants.GOOD_VALORATION_DISTRICT, ResourcesCompat.getDrawable(getResources(), R.drawable.portrait_default, null),
                    ResourcesCompat.getDrawable(getResources(), R.drawable.icon_unvote_login, null)));
        }
    }
}
