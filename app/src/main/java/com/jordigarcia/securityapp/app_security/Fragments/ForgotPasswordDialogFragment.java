package com.jordigarcia.securityapp.app_security.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.jordigarcia.securityapp.app_security.R;

/**
 * Created by Jordi on 21/03/2018.
 */

@SuppressLint("ValidFragment")
public class ForgotPasswordDialogFragment extends android.support.v4.app.DialogFragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        //getDialog().getWindow().setDimAmount(0f);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog_forgot_password, container, false);

        TextView resendButton = (TextView) v.findViewById(R.id.textview_resend_password);
        TextView closeButton = (TextView)v.findViewById(R.id.textview_close_forgot_pass_dialog);
        EditText emailText = (EditText) v.findViewById(R.id.edittext_mail_forgot_password);

        ((InputMethodManager) emailText.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(
                emailText, InputMethodManager.SHOW_IMPLICIT);

        resendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.layout_launch), Html.fromHtml("<font color=\"#713CA3\">Password sent. Please, check your email address.</font>"), Snackbar.LENGTH_LONG);
                snackbar.getView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
                snackbar.show();
                getDialog().dismiss();
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return v;
    }

}
