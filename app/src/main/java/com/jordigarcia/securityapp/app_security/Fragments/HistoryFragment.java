package com.jordigarcia.securityapp.app_security.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jordigarcia.securityapp.app_security.Adapters.RecyclerViewHistoryListAdapter;
import com.jordigarcia.securityapp.app_security.Utils.Constants;
import com.jordigarcia.securityapp.app_security.HistoryEntry;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.TitleHistory;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

/**
 * Created by Jordi on 02/04/2018.
 */

public class HistoryFragment  extends Fragment{

    private RecyclerView recyclerViewListHistory;
    private RecyclerViewHistoryListAdapter recyclerViewListHistoryAdapter;
    private List<Object> historyEntries;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history_profile_city_district, container, false);

        //RecyclerView
        recyclerViewListHistory = (RecyclerView)rootView.findViewById(R.id.rv_history_profile);
        recyclerViewListHistory.setHasFixedSize(true);

        historyEntries = new ArrayList<>();
        loadHistoryData();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewListHistory.setLayoutManager(layoutManager);

        recyclerViewListHistoryAdapter = new RecyclerViewHistoryListAdapter(getContext(), historyEntries);
        ScaleInAnimationAdapter alphaAdapter = new ScaleInAnimationAdapter(recyclerViewListHistoryAdapter);
        alphaAdapter.setFirstOnly(false);
        recyclerViewListHistory.setAdapter(alphaAdapter);

        return rootView;
    }

    public void loadHistoryData(){
        historyEntries.add(null); //This is crap, i have to fixed. When position 0 it shows the header but it skips the 0 position in array. It happens in all the adapters in the app
        historyEntries.add(new TitleHistory("Recent city votes"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_CITY,"Inverness", "United Kingdom","27.05.1994"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_CITY,"Inverness", "United Kingdom","27.05.1994"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_CITY,"Inverness", "United Kingdom","27.05.1994"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_CITY,"Inverness", "United Kingdom","27.05.1994"));
        historyEntries.add(new TitleHistory("Recent district votes"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_DISTRICT,"Merkinch", "Inverness, UK","01.01.2018"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_DISTRICT,"Merkinch", "Inverness, UK","01.01.2018"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_DISTRICT,"Merkinch", "Inverness, UK","01.01.2018"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_DISTRICT,"Merkinch", "Inverness, UK","01.01.2018"));
        historyEntries.add(new TitleHistory("Recent country votes"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_COUNTRY,"United Kingdom", "","02.04.2018"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_COUNTRY,"United Kingdom", "","02.04.2018"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_COUNTRY,"United Kingdom", "","02.04.2018"));
        historyEntries.add(new HistoryEntry(Constants.HOLDER_TYPE_HISTORY_COUNTRY,"United Kingdom", "","02.04.2018"));

    }

}
