package com.jordigarcia.securityapp.app_security.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jordigarcia.securityapp.app_security.Activities.HomeActivity;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.UserState;
import com.jordigarcia.securityapp.app_security.Utils.Constants;

import org.w3c.dom.Text;

/**
 * Created by Jordi on 17/03/2018.
 */

public class LogInFragment extends android.support.v4.app.DialogFragment {

    private Button buttonLogIn;
    private EditText textEmail, textPassword;
    private TextView textForgotPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_log_in, container, false);
        buttonLogIn = (Button)rootView.findViewById(R.id.button_log_in_end);
        textEmail = (EditText)rootView.findViewById(R.id.edittext_email_login);
        textPassword = (EditText)rootView.findViewById(R.id.edittext_password_login);
        textForgotPassword = (TextView)rootView.findViewById(R.id.button_forgot_password);


        buttonLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserState.getInstance().setLogState(Constants.USER_LOGGED_IN);
                Intent intent = new Intent(getContext(), HomeActivity.class);
                getContext().startActivity(intent);
            }
        });

        textForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ForgotPasswordDialogFragment dialog = new ForgotPasswordDialogFragment();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "forgot_password_fragment");
            }
        });



        return rootView;
    }
}
