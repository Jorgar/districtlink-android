package com.jordigarcia.securityapp.app_security.Fragments;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jordigarcia.securityapp.app_security.Activities.HomeActivity;
import com.jordigarcia.securityapp.app_security.Activities.PaymentActivity;
import com.jordigarcia.securityapp.app_security.R;

/**
 * Created by Jordi on 02/04/2018.
 */

public class PremiumFragment  extends Fragment {

    private ImageView premiumAd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_premium_profile, container, false);

        premiumAd = (ImageView) rootView.findViewById(R.id.imageview_premium);

        premiumAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), PaymentActivity.class);
                getContext().startActivity(intent);
            }
        });

        return rootView;
    }

}
