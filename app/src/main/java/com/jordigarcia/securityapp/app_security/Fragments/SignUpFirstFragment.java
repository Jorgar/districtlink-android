package com.jordigarcia.securityapp.app_security.Fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jordigarcia.securityapp.app_security.Interfaces.GoToNextInterface;
import com.jordigarcia.securityapp.app_security.R;

/**
 * Created by Jordi on 15/03/2018.
 */

public class SignUpFirstFragment extends Fragment {

    private Button buttonSignUp;
    private EditText textUsername, textEmail;
    private ViewPager viewPager;

    private GoToNextInterface mInterface;

    public void setInterface(GoToNextInterface mInterface) {
        this.mInterface = mInterface;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_up_1, container, false);
        buttonSignUp = (Button)rootView.findViewById(R.id.button_next_sign_up);
        textUsername = (EditText)rootView.findViewById(R.id.edittext_name_signup);
        textEmail = (EditText)rootView.findViewById(R.id.edittext_email_signup);

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInterface != null) {
                    mInterface.onGoToNext();
                }
            }
        });

        return rootView;
    }
}
