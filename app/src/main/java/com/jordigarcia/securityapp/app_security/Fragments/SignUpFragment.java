package com.jordigarcia.securityapp.app_security.Fragments;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.jordigarcia.securityapp.app_security.Adapters.ScreenSlidePagerAdapter;
import com.jordigarcia.securityapp.app_security.Interfaces.GoToNextInterface;
import com.jordigarcia.securityapp.app_security.NoSwipeableViewPager;
import com.jordigarcia.securityapp.app_security.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jordi on 03/03/2018.
 */

public class SignUpFragment extends Fragment implements GoToNextInterface{

    private NoSwipeableViewPager vPager;
    private PagerAdapter vPagerAdapter;
    private List<Fragment> fragmentList = new ArrayList<>();

    @Override
    public void onGoToNext() {
        if (vPager != null) {
            if(vPager.getCurrentItem() == 0) {
                vPager.setCurrentItem(vPager.getCurrentItem() + 1);
            }else if (vPager.getCurrentItem() == 1){
                vPager.setCurrentItem(vPager.getCurrentItem() - 1);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_up_menu, container, false);

        // Instantiate a ViewPager and a PagerAdapter.

        SignUpFirstFragment fragment1 = new SignUpFirstFragment();
        SignUpSecondFragment fragment2 = new SignUpSecondFragment();
        fragment1.setInterface(this);
        fragment2.setInterface(this);

        fragmentList.add(fragment1);
        fragmentList.add(fragment2);
        vPager = (NoSwipeableViewPager) rootView.findViewById(R.id.viewpage_sign_up);
        vPagerAdapter = new ScreenSlidePagerAdapter(getContext(), this.getFragmentManager(),fragmentList);
        vPager.setAdapter(vPagerAdapter);
        return rootView;
    }

}


