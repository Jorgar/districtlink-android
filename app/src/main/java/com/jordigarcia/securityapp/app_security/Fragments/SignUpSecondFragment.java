package com.jordigarcia.securityapp.app_security.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jordigarcia.securityapp.app_security.Activities.HomeActivity;
import com.jordigarcia.securityapp.app_security.Activities.MapListActivity;
import com.jordigarcia.securityapp.app_security.Interfaces.GoToNextInterface;
import com.jordigarcia.securityapp.app_security.R;
import com.jordigarcia.securityapp.app_security.UserState;
import com.jordigarcia.securityapp.app_security.Utils.Constants;

/**
 * Created by Jordi on 15/03/2018.
 */

public class SignUpSecondFragment extends Fragment {

    private Button buttonSignUp, buttonBack;
    private EditText password, retypePassword;

    private GoToNextInterface mInterface;

    public void setInterface(GoToNextInterface mInterface) {
        this.mInterface = mInterface;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_up_2, container, false);
        buttonSignUp = (Button)rootView.findViewById(R.id.button_sign_up);
        buttonBack = (Button)rootView.findViewById(R.id.button_back_sign_up);
        password = (EditText)rootView.findViewById(R.id.edittext_password_signup);
        retypePassword = (EditText)rootView.findViewById(R.id.edittext_retype_password_signup);

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInterface != null) {
                    mInterface.onGoToNext();
                }
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserState.getInstance().setLogState(Constants.USER_LOGGED_IN);
                Intent intent = new Intent(getContext(), HomeActivity.class);
                getContext().startActivity(intent);
            }
        });

        return rootView;
    }
}
