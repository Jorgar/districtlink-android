package com.jordigarcia.securityapp.app_security;

/**
 * Created by Jordi on 02/04/2018.
 */

public class HistoryEntry {

    private int type;
    private String title, subtitle, date;

    public HistoryEntry(int type, String title, String subtitle, String date) {
        this.type = type;
        this.title = title;
        this.subtitle = subtitle;
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
