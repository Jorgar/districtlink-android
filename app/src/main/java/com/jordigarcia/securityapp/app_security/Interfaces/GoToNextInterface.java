package com.jordigarcia.securityapp.app_security.Interfaces;

/**
 * Created by Jordi on 15/03/2018.
 */

public interface GoToNextInterface {
    void onGoToNext();
}