package com.jordigarcia.securityapp.app_security;

import android.graphics.drawable.Drawable;

public class TitleCountry {

    private String titleCountry;
    private Drawable iconCountry;

    public TitleCountry(String titleCountry, Drawable iconCountry) {
        this.titleCountry = titleCountry;
        this.iconCountry = iconCountry;
    }

    public String getTitleCountry() {
        return titleCountry;
    }

    public void setTitleCountry(String titleCountry) {
        this.titleCountry = titleCountry;
    }

    public Drawable getIconCountry() {
        return iconCountry;
    }

    public void setIconCountry(Drawable iconCountry) {
        this.iconCountry = iconCountry;
    }
}
