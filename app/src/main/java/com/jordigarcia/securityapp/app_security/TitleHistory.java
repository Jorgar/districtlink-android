package com.jordigarcia.securityapp.app_security;

/**
 * Created by Jordi on 02/04/2018.
 */

public class TitleHistory {

    private String title;

    public TitleHistory(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
