package com.jordigarcia.securityapp.app_security;

import com.jordigarcia.securityapp.app_security.Utils.Constants;

public class UserState {

    private static int logState = Constants.USER_NOT_REGISTERED;

    private static final UserState ourInstance = new UserState();

    public static UserState getInstance() {
        return ourInstance;
    }

    private UserState() {

    }

    public static int getLogState() {
        return logState;
    }

    public static void setLogState(int logState) {
        UserState.logState = logState;
    }
}
