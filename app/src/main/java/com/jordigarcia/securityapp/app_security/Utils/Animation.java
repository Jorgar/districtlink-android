package com.jordigarcia.securityapp.app_security.Utils;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by Jordi on 18/04/2018.
 */

public class Animation {

    public static void loadAnimatedFragment(FragmentActivity fContext, int idHolder, Fragment fragmentToLoad, int idAnimIn, int idAnimOut){
        FragmentManager fragmentManager = fContext.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(idAnimIn, idAnimOut,
                idAnimIn, idAnimOut);
        fragmentTransaction.add(idHolder, fragmentToLoad);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
