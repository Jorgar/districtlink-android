package com.jordigarcia.securityapp.app_security.Utils;

import android.graphics.Color;

/**
 * Created by Jordi on 18/02/2018.
 */

public class Constants {

    //colors
    public static final int FILL_RED = Color.argb(60, 255, 0, 0);
    public static  final int FILL_YELLOW = Color.argb(80, 150, 150, 0);
    public static final int FILL_GREEN = Color.argb(80, 39, 132, 36);

    public static final int SAFE_SECURITY_LEVEL = 2;
    public static final int MEDIUM_SECURITY_LEVEL = 1;
    public static final int UNSAFE_SECURITY_LEVEL = 0;

    public static final int GOOD_VALORATION_DISTRICT = 1;
    public static final int BAD_VALORATION_DISTRICT = -1;
    public static final int NEUTRAL_VALORATION_DISTRICT = 0;

    public static final int HOLDER_TYPE_HISTORY_DISTRICT = 0;
    public static final int HOLDER_TYPE_HISTORY_CITY = 1;
    public static final int HOLDER_TYPE_HISTORY_COUNTRY = 2;

    public static final int HOLDER_TYPE_LIST_DISTRICT = 0;
    public static final int HOLDER_TYPE_LIST_CITY = 1;
    public static final int HOLDER_TYPE_LIST_COUNTRY = 2;

    public static final int LIST_DISTRICT = 0;
    public static final int LIST_CITY = 1;
    public static final int LIST_COUNTRY = 2;

    public static final int USER_LOGGED_IN = 0;
    public static final int USER_NOT_REGISTERED = 1;

    public static final int CITY_TYPE_HOLDER= 0;
    public static final int DISTRICT_TYPE_HOLDER = 1;

    public static final int CITY_ACTIVITY = 0;
    public static final int DISTRICT_ACTIVITY = 1;
    public static final int COUNTRY_ACTIVITY = 2;

    public static int MAP_DISTRICT = 0;
    public static int MAP_CITY = 1;
    public static int MAP_COUNTRY = 2;


    //Districts
    public static int UK_INVERNESS_DISTRICTS = 0;

    //Cities
    public static int UK_CITIES = 0;

    //Countries
    public static int UK_COUNTRY = 0;


}
